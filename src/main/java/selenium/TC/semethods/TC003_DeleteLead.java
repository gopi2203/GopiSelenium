package selenium.TC.semethods;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TC003_DeleteLead extends SeMethods {
	@BeforeClass
	public void setdata() {
		testCaseName = "TC001";
		testCaseDescription =  "Create Lead";
		author =  "Gopi";
		category = "smoke";

	}
	@Test
	public void DeleteLead()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickLink = locateElement("linktext", "CRM/SFA");
		click(clickLink);
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,"CISCO");
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,"Gopi");
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,"Ekambaram");
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,"Automobile");
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement getName = locateElement("name","firstName");
		getText(getName);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");
		WebElement getUserId = locateElement("xpath", "//span[@id='viewLead_companyName_sp']");
		String text = getText(getUserId);
		String correctText = text.replaceAll("[^0-9]", "");
		System.out.println(correctText);
		WebElement clickDelete = locateElement("xpath", "//a[text()='Delete']");
		click(clickDelete);
		WebElement clickFindLead = locateElement("xpath", "//a[text()='Find Leads']");
		click(clickFindLead);
		WebElement getId = locateElement("name","id");
		type(getId, correctText);
		WebElement clickFindLeadBut = locateElement("xpath", "//button[text()='Find Leads']");
		click(clickFindLeadBut);
		WebElement getOutput = locateElement("xpath", "//div[text()='No records to display']");
		getText(getOutput);
		verifyExactText(getOutput, "No records to display");



	}

}
