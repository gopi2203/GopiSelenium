package selenium.TC.semethods;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

public class TC004_MergeLead extends SeMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC004";
		testCaseDescription =  "Merge Lead";
		author =  "Gopi";
		category = "Smoke";
	}

	@Test
	public void MergeLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickLink = locateElement("linktext", "CRM/SFA");
		click(clickLink);
		WebElement clickLead = locateElement("linktext", "Leads");
		click(clickLead);
		WebElement clickCreateLead = locateElement("linktext", "Create Lead");
		click(clickCreateLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,"CISCO");
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,"Gopi");
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,"E");
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,"Automobile");
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement getUserId = locateElement("xpath", "//span[@id='viewLead_companyName_sp']");
		String text = getText(getUserId);
		String correctText = text.replaceAll("[^0-9]", "");
		System.out.println(correctText);
		WebElement clickSecCreateLead = locateElement("linktext", "Create Lead");
		click(clickSecCreateLead);
		WebElement setSecCompName = locateElement("id", "createLeadForm_companyName");
		type(setSecCompName,"HCL");
		WebElement setSecFirstName = locateElement("id", "createLeadForm_firstName");
		type(setSecFirstName,"Gopi");
		WebElement setSecLastName = locateElement("id", "createLeadForm_lastName");
		type(setSecLastName,"Ekambaram");
		WebElement SecmarkCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(SecmarkCampDropDown,"Automobile");
		WebElement submitSecLead = locateElement("class", "smallSubmit");
		click(submitSecLead);
		WebElement getSecUserId = locateElement("xpath", "//span[@id='viewLead_companyName_sp']");
		String secText = getText(getSecUserId);
		String correctSecText = secText.replaceAll("[^0-9]", "");
		System.out.println(correctSecText);
		WebElement clickMerge = locateElement("linktext", "Merge Leads");
		click(clickMerge);
		WebElement clickMerIcon = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img");
		click(clickMerIcon);
		switchToWindow(1);
		WebElement typeID = locateElement("name", "id");
		type(typeID, correctText);
		WebElement buttonLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonLead);
		Thread.sleep(3000);
		WebElement clickFirstEle = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(clickFirstEle);
		switchToWindow(0);
		Thread.sleep(2000);
		WebElement clickMerIcon1 = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img");
		clickWithNoSnap(clickMerIcon1);
		switchToWindow(1);
		WebElement typeIDTo = locateElement("name", "id");
		type(typeIDTo, correctSecText);
		WebElement buttonLeadTo = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonLeadTo);
		WebElement clickFirstEleTo = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(clickFirstEleTo);
		switchToWindow(0);
		Thread.sleep(3000);
		WebElement clickMergeIcon = locateElement("xpath", "//a[text()='Merge']");
		clickWithNoSnap(clickMergeIcon);
		Thread.sleep(3000);
		acceptAlert();
		Thread.sleep(2000);
		WebElement findLead = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLead);
		Thread.sleep(3000);
		WebElement typeLeadID = locateElement("name", "id");
		type(typeLeadID, correctText);
		WebElement buttonFindLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonFindLead);
		WebElement getOutput = locateElement("xpath", "//div[text()='No records to display']");
		getText(getOutput);
		verifyExactText(getOutput, "No records to display");
	}


}
