package selenium.TC.semethods;
import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TC002_EditLead extends SeMethods {

	public void EditLead()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickLink = locateElement("linktext", "CRM/SFA");
		click(clickLink);
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,"HCL");
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,"Gopinath");
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,"Ekambaram");
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,"Car and Driver");
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");
		WebElement clickEdit = locateElement("linktext", "Edit");
		click(clickEdit);
		WebElement setTitle = locateElement("id","updateLeadForm_generalProfTitle");
		type(setTitle,"Editing the existing Lead");
		WebElement clickUpdate = locateElement("xpath","//input[@value='Update']");
		click(clickUpdate);
		WebElement printText = locateElement("id", "viewLead");
		getText(printText);
		verifyPartialText(printText,"Editing the existing Lead");
	}

	
}