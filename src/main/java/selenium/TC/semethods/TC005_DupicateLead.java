package selenium.TC.semethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

public class TC005_DupicateLead extends SeMethods {

	@BeforeClass
	public void setdata() {
		testCaseName = "TC005";
		testCaseDescription =  "Duplicate Lead";
		author =  "Gopi";
		category = "Smoke";
	}


	@Test
	public void DuplicateLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickLink = locateElement("linktext", "CRM/SFA");
		click(clickLink);
		WebElement clickLead = locateElement("linktext", "Leads");
		click(clickLead);
		WebElement clickCreateLead = locateElement("linktext", "Create Lead");
		click(clickCreateLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,"CISCO");
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,"Gopi");
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,"Ekambaram");
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,"Automobile");
		WebElement setEmail = locateElement("id", "createLeadForm_primaryEmail");
		type(setEmail,"gopipj022@gmail.com");
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement getEmail = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String secText = getText(getEmail);
//		String correctSecText = secText.replaceAll("[^0-9]", "");
		System.out.println(secText);
		WebElement clickLeads = locateElement("linktext", "Find Leads");
		click(clickLeads);
		WebElement clickEmail = locateElement("xpath", "//span[text()='Email']");
		click(clickEmail);
		WebElement eleEmail = locateElement("name", "emailAddress");
		type(eleEmail, secText);
		WebElement clickFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(clickFindLeads);
		Thread.sleep(3000);
		WebElement clickFirstele = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(clickFirstele);
		WebElement clickDupLeads = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(clickDupLeads);
		WebElement eleTitle = locateElement("id","createLeadForm_generalProfTitle");
		type(eleTitle, "Welcome Gopi");
		WebElement clickSubCreate = locateElement("name", "submitButton");
		click(clickSubCreate);
		WebElement getTitle = locateElement("id","viewLead_generalProfTitle_sp");
		getText(getTitle);
		verifyExactText(getTitle, "Welcome Gopi");



	}

}
