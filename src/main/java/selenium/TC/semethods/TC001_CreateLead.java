package selenium.TC.semethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

public class TC001_CreateLead extends SeMethods {
	
	@BeforeClass
	public void setdata() {
				testCaseName = "TC001";
				testCaseDescription =  "Create Lead";
				author =  "Gopi";
				category = "smoke";
				
	}
@Test
	public void CreateLead(){
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickLink = locateElement("linktext", "CRM/SFA");
		click(clickLink);
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,"CISCO");
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,"Gopi");
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,"E");
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,"Automobile");
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement printText = locateElement("id", "viewLead");
		getText(printText);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");


	}





}
