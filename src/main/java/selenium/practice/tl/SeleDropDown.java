package selenium.practice.tl;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleDropDown {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Dropdown.html");
		WebElement dd = driver.findElementById("dropdown1");
		Select index = new Select(dd);
		index.selectByIndex(1);
		Thread.sleep(3000);
		
		WebElement dd1 = driver.findElementByName("dropdown2");
		Select value = new Select(dd1);
		value.selectByValue("2");
		
		WebElement dd2 = driver.findElementById("dropdown3");
		Select text = new Select(dd2);
		text.selectByVisibleText("Loadrunner");
		
		WebElement dd3 = driver.findElementByClassName("dropdown");
		Select count = new Select(dd3);
		
		List<WebElement> countOptions = count.getOptions();
		System.out.println("The number of options in the dropdown is " +countOptions.size());
		
		driver.findElementByXPath("(//div[@class='example']//select)[5]").sendKeys("Appium", Keys.TAB);
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='example']//select)[6]").sendKeys("UFT/QTP", Keys.TAB);
		
		
		
		


	}

}
