package selenium.practice.tl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleCheckBox {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/checkbox.html");
		Thread.sleep(2000);
		driver.findElementByXPath("(//input[@type='checkbox'])[3]").click();
		
		boolean selected = driver.findElementByXPath("(//input[@type='checkbox'])[6]").isSelected();
		if(selected)
		{
			System.out.println("the required checkbox is selected ");
		}
		else
		{
			System.out.println("the required checkbox is not selected ");
			driver.findElementByXPath("(//input[@type='checkbox'])[6]").click();
		}
		Thread.sleep(3000);
		driver.findElementByXPath("(//input[@type='checkbox'])[8]").click();

		List<WebElement> checkBox = driver.findElements(By.xpath("(//input[@type='checkbox'])"));
		System.out.println(checkBox.size());
		for(int i=8;i<checkBox.size();i++)
		{
			checkBox.get(i).click();
			System.out.println(checkBox.get(i).getText());
		}
//		List<WebElement> boxCount = driver.findElements(By.xpath("(//label[@for='java'])[4]"));
//		System.out.println(boxCount.size());
//		for(int i=0;i<boxCount.size();i++)
//		{
//			System.out.println(boxCount.get(i).getText());
//			boxCount.get(i).click();
//		}

		//		for (String string : args) {
		//			
		//		}		
		//		driver.findElementByXPath("(//input[@type='checkbox'])[9]").click();
		//		driver.findElementByXPath("(//input[@type='checkbox'])[10]").click();
		//		driver.findElementByXPath("(//input[@type='checkbox'])[11]").click();
		//		driver.findElementByXPath("(//input[@type='checkbox'])[12]").click();
		//		driver.findElementByXPath("(//input[@type='checkbox'])[13]").click();
		//		driver.findElementByXPath("(//input[@type='checkbox'])[14]").click();






	}

}
