package selenium.practice.tl;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcDropDown {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		Thread.sleep(3000);
		WebElement secuQn = driver.findElementById("userRegistrationForm:securityQ");
		Select dd = new Select(secuQn);
		List<WebElement> options = dd.getOptions();
		System.out.println(options.size());
		Thread.sleep(3000);
		for (WebElement allOptions : options) {
			System.out.println(allOptions.getText());
		}
		
//		for(int i=0;i<=options.size();i++) {
//			System.out.println(secuQn(i));
//		}

	}

}
