package selenium.practice.tl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img").click();
		Thread.sleep(3000);
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		System.out.println("Count of the windows is: "+allWindows.size());
		List<String> window1 = new ArrayList<String>();
		window1.addAll(allWindows);
		String secWindow = window1.get(1);
		driver.switchTo().window(secWindow);
		driver.findElementByName("id").sendKeys("10559");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(parentWindow);
		Thread.sleep(3000);
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img").click();
		for (String childWind : driver.getWindowHandles()) {
			driver.switchTo().window(childWind);
		}
		System.out.println(driver.getTitle());
		Thread.sleep(3000);
		driver.findElementByName("id").sendKeys("10657");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(parentWindow);
		System.out.println(driver.getTitle());
		Thread.sleep(3000);
		driver.findElementByXPath("//a[text()='Merge']").click();
		driver.switchTo().alert().accept();
		Thread.sleep(3000);
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByName("id").sendKeys("10559");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		File src=driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snapShot/img.png");
		FileUtils.copyFile(src, dest);
		Thread.sleep(8000);
		driver.close();
		
		
		
		
		
		
		
		

	}

}
