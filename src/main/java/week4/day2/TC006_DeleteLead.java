package week4.day2;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TC006_DeleteLead extends ProjectMethods {
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName = "TC006";
		testCaseDescription = "Merge Lead";
		author = "Gopi";
		category = "regression";
	}
//	@Test(groups = "regression", dependsOnGroups = "sanity",dataProvider="deleteLead")
	@Test(dataProvider="deleteLead")
	@Parameters("deleteLead")
	public void DeleteLead(String compName,String firstName,String lastName,String marketingCampaign)
	{
		
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,compName);
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,firstName);
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,lastName);
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,marketingCampaign);
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement getName = locateElement("name","firstName");
		getText(getName);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");
		WebElement clickDelete = locateElement("xpath", "//a[text()='Delete']");
		click(clickDelete);
		WebElement clickFindLead = locateElement("xpath", "//a[text()='Find Leads']");
		click(clickFindLead);
		
		
		
	}
	@DataProvider(name="deleteLead")
	public Object[][] getData() {
		Object [][] data = new Object [3][4];
				data[0][0]="HCL";
				data[0][1]="Gopi";
				data[0][2]="Ekambaram";
				data[0][3]="Automobile";
				
				data[1][0]="Cisco";
				data[1][1]="Geka";
				data[1][2]="bar";
				data[1][3]="Automobile";
				
				data[2][0]="Cisco";
				data[2][1]="Aasa";
				data[2][2]="Priyanka";
				data[2][3]="Automobile";
				return data;
				}
}
