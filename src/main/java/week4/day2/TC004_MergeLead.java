package week4.day2;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

public class TC004_MergeLead extends ProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC004";
		testCaseDescription =  "Merge Lead";
		author =  "Gopi";
		category = "Smoke";
	}

	@Test(dataProvider = "mergeLead")
	@Parameters("mergeLead")
	public void MergeLead(String userIDOne, String userIDTwo, String userIDVeri) throws InterruptedException {
		WebElement clickLead = locateElement("linktext", "Leads");
		click(clickLead);
		WebElement clickMerge = locateElement("linktext", "Merge Leads");
		click(clickMerge);
		WebElement clickMerIcon = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img");
		click(clickMerIcon);
		switchToWindow(1);
		WebElement typeID = locateElement("name", "id");
		type(typeID, userIDOne);
		WebElement buttonLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonLead);
		Thread.sleep(3000);
		WebElement clickFirstEle = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(clickFirstEle);
		Thread.sleep(3000);
		switchToWindow(0);
		Thread.sleep(2000);
		WebElement clickMerIcon1 = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img");
		clickWithNoSnap(clickMerIcon1);
		switchToWindow(1);
		WebElement typeIDTo = locateElement("name", "id");
		type(typeIDTo, userIDTwo);
		WebElement buttonLeadTo = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonLeadTo);
		WebElement clickFirstEleTo = locateElement("xpath", "(//a[@class='linktext'])[1]");
		click(clickFirstEleTo);
		Thread.sleep(3000);
		WebElement clickMergeIcon = locateElement("xpath", "//a[text()='Merge']");
		click(clickMergeIcon);
		Thread.sleep(3000);
		acceptAlert();
		WebElement findLead = locateElement("xpath", "//a[text()='Find Leads']");
		type(findLead, userIDVeri);
		click(buttonLead);
		WebElement getOutput = locateElement("xpath", "//div[text()='No records to display']");
		getText(getOutput);
		verifyExactText(getOutput, "No Records to display");
	}
	@DataProvider(name="mergeLead")
	public Object[][] getData() {
		Object [][] data = new Object [3][3];
				data[0][0]="10551";
				data[0][1]="10888";
				data[0][2]="10551";
	return data;
	}
	
}
