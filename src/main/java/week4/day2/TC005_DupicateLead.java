package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

public class TC005_DupicateLead extends ProjectMethods {

	@BeforeClass
	public void setdata() {
				testCaseName = "TC005";
				testCaseDescription =  "Duplicate Lead";
				author =  "Gopi";
				category = "Smoke";
	}


	@Test
	public void DuplicateLead() throws InterruptedException {
		WebElement clickLead = locateElement("linktext", "Leads");
		click(clickLead);
		WebElement clickMerge = locateElement("linktext", "Find Leads");
		click(clickMerge);
		WebElement clickEmail = locateElement("xpath", "//span[text()='Email']");
		click(clickEmail);
		WebElement eleEmail = locateElement("name", "emailAddress");
		type(eleEmail, "gopipj022@gmail.com");
		WebElement clickFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(clickFindLeads);
		Thread.sleep(3000);
		WebElement clickFirstele = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(clickFirstele);
		WebElement clickDupLeads = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(clickDupLeads);
		WebElement eleTitle = locateElement("id","createLeadForm_generalProfTitle");
		type(eleTitle, "Welcome Gopi");
		WebElement clickSubCreate = locateElement("name", "submitButton");
		click(clickSubCreate);
		WebElement getTitle = locateElement("id","viewLead_generalProfTitle_sp");
		getText(getTitle);
		verifyExactText(getTitle, "Welcome Gopi");



	}

}
