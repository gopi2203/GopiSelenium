package week4.day2;
import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TC003_EditLead extends ProjectMethods {
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName = "TC003";
		testCaseDescription =  "Merge Lead";
		author =  "Gopi";
		category = "sanity";
	}
//	@Test(groups = "sanity", dependsOnGroups = "smoke")
	@Test(dataProvider="editLead")
	@Parameters("editLead")
	public void EditLead(String compName,String firstName,String lastName,String marketingCampaign)
	{
		
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,compName);
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,firstName);
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,lastName);
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,marketingCampaign);
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");
		WebElement clickEdit = locateElement("linktext", "Edit");
		click(clickEdit);
		WebElement setTitle = locateElement("id","updateLeadForm_generalProfTitle");
		type(setTitle,"Editing the existing Lead");
		WebElement clickUpdate = locateElement("xpath","//input[@value='Update']");
		click(clickUpdate);
		WebElement printText = locateElement("id", "viewLead");
		getText(printText);
		verifyPartialText(printText,"Editing the existing Lead");
	}

	@DataProvider(name="editLead")
	public Object[][] getData() {
		Object [][] data = new Object [3][4];
				data[0][0]="HCL";
				data[0][1]="Gopi";
				data[0][2]="Ekambaram";
				data[0][3]="Automobile";
				
				data[1][0]="Cisco";
				data[1][1]="Geka";
				data[1][2]="bar";
				data[1][3]="Automobile";
				
				data[2][0]="Cisco";
				data[2][1]="Aasa";
				data[2][2]="Priyanka";
				data[2][3]="Automobile";
				return data;
				}
}