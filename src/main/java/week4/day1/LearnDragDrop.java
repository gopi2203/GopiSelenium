package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnDragDrop {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementByXPath("//div[@id='draggable']");
		//		WebElement drop = driver.findElementById("//div[@id='droppable']");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(drag, 100, 200).perform();

	}

}
