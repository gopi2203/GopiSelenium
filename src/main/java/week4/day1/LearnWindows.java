package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		Thread.sleep(5000);
		driver.findElementByLinkText("AGENT LOGIN").click();
		Thread.sleep(5000);
		driver.findElementByLinkText("Contact Us").click();
		Thread.sleep(5000);
		Set<String> allWindows = driver.getWindowHandles();
		System.out.println("Count of the windows is: "+allWindows.size());
		List<String> getWindows = new ArrayList<String>();
		getWindows.addAll(allWindows);
		System.out.println("Printing all the windows: "+getWindows);
		String secondWindow = getWindows.get(1);
		driver.switchTo().window(secondWindow);
		System.out.println("Title of the window is : "+driver.getTitle());
		System.out.println("Current URL is: "+driver.getCurrentUrl());
		driver.quit();
	}
}
