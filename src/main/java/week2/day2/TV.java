package week2.day2;

public interface TV {
    public void changeChannel(int byNum);
    
    public void changeChannel(String byName);
    
    public void channelOn(boolean isOn);
    
}
