package com.gopi;

import java.util.Scanner;

public class SecLargestOf4Nos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        int p = 0;
        int q = 0;
        int r = 0;
        int SecondBig = 0;
		Scanner gp = new Scanner(System.in);
		
		System.out.println("Enter the first no: ");
		int a = gp.nextInt();
		System.out.println("Enter the second no: ");
		int b = gp.nextInt();
		System.out.println("Enter the third no: ");
		int c = gp.nextInt();
		System.out.println("Enter the fourth no: ");
		int d = gp.nextInt();
		if(a > b && a > c && a > d)
		{
			System.out.println(a+ " is greater");
			p = b;
			q = c;
		    r = d;
		}
		else if(b > a && b > c && b > d)
		{
			System.out.println(b+ " is greater");
			p = a;
			q = c;
		    r = d;
		}
		else if(c > a && c > b && c > d)
		{
			System.out.println(c+ " is greater");
			p = a;
			q = b;
		    r = d;
		}
		else if(d > a && d > c && d > b)
		{
			System.out.println(d+ " is greater");
			p = a;
			q = b;
		    r = c;
		}
		else
		{
			System.out.println("Invalid values");
		}
		if(p > q && p > r)
		{
		    SecondBig = p;
		}
		else if(q > p && q > r)
		{
			SecondBig = q;
		}
		else if(r > p && r > q)
		{
			SecondBig = r;
		}
		System.out.println("Second largest no is: " +SecondBig);
		gp.close();
	}

}
