package com.gopi;

import java.util.Scanner;

public class FibonacciSeries {
	
	public static void main(String[] args) {
		System.out.println("Enter the value of n");
		Scanner scan = new Scanner(System.in);
		int val = scan.nextInt();
		
		int  a= 0, b=1;
		System.out.println(a+" "+b);
		for(int i=2;i<val;i++) {
			int sum = a + b;
			a = b;
			b = sum;
			
			
			System.out.println(" "+sum);
			
//			scan.close();
		}
		
	}

}
