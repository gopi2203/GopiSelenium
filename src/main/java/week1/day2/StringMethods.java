package week1.day2;

public class StringMethods {
	
	static String name = "Gopinath";
	
	static int length = name.length();
	static char ch = name.charAt(5);
	static String sub = name.substring(0, 3);
	static String tr = name.trim();
	static char allChar[] = name.toCharArray();
	static String[] spl = name.split("pi");
	static String repl = name.replace("o", "oo");
	static String conc = name.concat("a");
	static boolean findText = name.contains("w");
	static boolean startText = name.startsWith("G");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(length);
		System.out.println(ch);
		System.out.println(sub);
		System.out.println(tr);
		for (char charc : allChar) {
			System.out.println(charc);
		}
		for (String spli : spl) {
			System.out.println(spli);
		}
		System.out.println(repl);
		System.out.println(conc);
		System.out.println(findText);
		System.out.println(startText);
	}

}

