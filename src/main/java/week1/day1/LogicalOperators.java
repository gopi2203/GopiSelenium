package week1.day1;

public class LogicalOperators {
	static  int a = 30;
	static  int b = 80;
	static  int c = 20;
	
	static boolean res = (a<b) & (a<c); //and
	static boolean res1 = (a>b) != (a<c); //not equal to
	static boolean res2 = (a<b) && (a>c); //conditional and
	static boolean res3 = (a<b) || (a<c); //conditional or
	static boolean res4 = (a<b) | (a<c); //or
	static boolean res5 = (a<b) ^ (a<c); //xor
	static boolean res6 = (a<b) == (a>c);
	public static void main(String[] args) {
		
		System.out.println(res);
		System.out.println(res1);
		System.out.println(res2);
		System.out.println(res3);
		System.out.println(res4);
		System.out.println(res5);
		System.out.println(res6);
	}

}
