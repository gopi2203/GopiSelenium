package week1.hw.dailychallange;

import java.util.*;

public class ArithOper3Inputs {

	public static void main(String[] args) {
		System.out.println("Enter the first number: ");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		System.out.println("Enter the Second number: ");
		int b = scan.nextInt();
		System.out.println("Enter the String: ");
		String c = scan.next();
		int d = 0;
		if(c.equals("Add"))
		{
			d = a + b;
		}
		else if(c.equals("Sub"))
		{
			d = a-b;
		}
		else if(c.equals("Mul"))
		{
			d = a*b;
		}
		else if(c.equals("Div"))
		{
			d = a/b;
		}
		else
		{
			System.out.println("Enter valid operation");
		}
		System.out.println("the " +c+ " of two numbers is: " +d);
	}

}
