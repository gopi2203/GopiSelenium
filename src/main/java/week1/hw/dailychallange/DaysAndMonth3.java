package week1.hw.dailychallange;

import java.util.Scanner;

public class DaysAndMonth3 {

	public static void main(String[] args) {
		 
		System.out.println("Enter the number:");
		String[] months = {"Jan" ,"Feb" ,"Mar" ,"Apr" ,"May" ,"Jun" ,"Jul" ,"Aug" ,"Sep" ,"Oct" ,"Nov" ,"Dec"};
		Scanner month = new Scanner(System.in); 
		int a = month.nextInt();
		
		if(a >= 1 && a <=12) {
			System.out.println("the month number is " +a+ " and the name of the month is " +months[a-1]);
		
			if(a == 1 || a == 3 || a == 5 || a == 7 || a == 8 || a == 10 || a == 12){
				System.out.println("the number of days in the month is 31 ");
			}
			else if(a == 4 || a == 6 || a == 9 || a == 11){
			System.out.println("the number of days in the month is 30");
		    }
			else if(a == 2){
			System.out.println("the number of days in the month is 28 or 29");
			}
		}
			else {
				System.out.println("Invalid values");
			}
		month.close();

	}

}	