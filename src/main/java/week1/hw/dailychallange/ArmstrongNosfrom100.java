package week1.hw.dailychallange;

import java.util.*;

public class ArmstrongNosfrom100 {

//	public static void main(String[] args) {
//		System.out.println("Enter the starting number: ");
//		Scanner scan = new Scanner(System.in);
//		int val = scan.nextInt();
//		System.out.println("Enter the ending number: ");
//		int val1 = scan.nextInt();
//		int temp1 = val;
//		int count = 0;
//		while(temp1>0)
//		{
//			temp1 = temp1/10;
//			count++;
//		}
//		System.out.println(count);
//		
//		for(int i=val; i<=val1; i++)
//		{
//			int temp=i;
//			int a, b = 0;
//			while(temp>0)
//			{
//				a = temp%10;
//				temp = temp/10;
//				b = b + (int) Math.pow(a, count);
//			}
//			if(i == b) {
//				System.out.println("the armstrong number from " +val+ " to " +val1+ " is " +i);
//			}
//
//		}
//
//	}
	
	public static void main(String args[])
	   {
	      int n, sum = 0, temp, remainder, digits = 0;
	 
	      Scanner in = new Scanner(System.in);
	      System.out.println("Input a number to check if it is an Armstrong number");      
	      n = in.nextInt();
	 
	      temp = n;
	     
	      // Count number of digits
	     
	      while (temp != 0) {
	         digits++;
	         temp = temp/10;
	      }
	 
	      temp = n;
	 
	      while (temp != 0) {
	         remainder = temp%10;
	         sum = sum + (int)Math.pow(remainder, digits);
	         temp = temp/10;
	      }
	 
	      if (n == sum)
	         System.out.println(n + " is an Armstrong number.");
	      else
	         System.out.println(n + " isn't an Armstrong number.");        
	   }

}	