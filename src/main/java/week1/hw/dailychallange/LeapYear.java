package week1.hw.dailychallange;

import java.util.*;

public class LeapYear {

	public static void main(String[] args) {
		System.out.println("Enter the Year: ");
		Scanner scan = new Scanner(System.in);
		int val = scan.nextInt();

		if((val%400 == 0) || (val%4 == 0) && (val%100 != 0))
		{
			System.out.println("the year " +val+ " is leap year");
		}
		else
		{
			System.out.println("the year " +val+ " is not a leap year");
		}

	}

}
