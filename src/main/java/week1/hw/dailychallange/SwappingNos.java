package week1.hw.dailychallange;

import java.util.*;

public class SwappingNos {

	public static void main(String[] args) {
		System.out.println("Enter the values before swapping ");
		Scanner scan = new Scanner(System.in);
		int x = scan.nextInt();
		int y = scan.nextInt();
		
		int temp;
		
//		temp = x;
//		x = y;
//		y = temp;
		
		x = x+y;
		y = x-y;
		x = x-y;
		
		System.out.println("After swapping x value is" +x);
		System.out.println("After swapping y value is" +y);

	}

}
