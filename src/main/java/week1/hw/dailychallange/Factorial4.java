package week1.hw.dailychallange;

import java.util.Scanner;

public class Factorial4 {

	public static void main(String[] args) {
	   int fact = 1;
	   System.out.println("Enter the number: ");
	   Scanner scan = new Scanner(System.in);
	   int val = scan.nextInt();
	   
	   for(int i=1;i<=val;i++) {
		   fact = fact*i;
	   }
	   System.out.println("the factorial of " +val+ " is " +fact);
	   scan.close();
	}
	
}
