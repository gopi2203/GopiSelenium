package week1.hw.dailychallange;

import java.util.*;

public class PrintInBetNos {

	public static void main(String[] args) {
		System.out.println("Enter the starting number: ");
		Scanner scan = new Scanner(System.in);
		int val = scan.nextInt();
		System.out.println("Enter the ending number: ");
		int val1 = scan.nextInt();
		
		
		for(int i=val; i<=val1;i++)
		{
			if((i % 5 == 0) && (i % 3 == 0))
			{
				System.out.println("FIZZBUZZ");
			}
			else if(i % 5 == 0)
			{
				System.out.println("BUZZ");
			}
			else if(i % 3 == 0)
			{
				System.out.println("FIZZ");
			}
			else if((i % 5 != 0) && (i % 3 !=0)) 
			{
				System.out.println(i);
			}
			
		}

	}

}
