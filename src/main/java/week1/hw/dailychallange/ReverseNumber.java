package week1.hw.dailychallange;

import java.util.*;

public class ReverseNumber {

	public static void main(String[] args) {
		System.out.println("Enter the Value: ");
		Scanner gp = new Scanner(System.in);
		int val = gp.nextInt();
		
		int temp =val;
		int a, b = 0;
		
			while(temp>0) {
				a = temp%10;
				temp = temp/10;
				b = b*10 + a;
			}
			System.out.println("the Reversed number of " +val+ " is " +b);
	}

}
