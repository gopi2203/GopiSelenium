package week1.hw.dailychallange;

import java.util.Scanner;

public class LargestOfThreeNos2 {

	public static void main(String[] args) {
		
Scanner gp = new Scanner(System.in);
		
		System.out.println("Enter the first no: ");
		int a = gp.nextInt();
		System.out.println("Enter the second no: ");
		int b = gp.nextInt();
		System.out.println("Enter the third no: ");
		int c = gp.nextInt();
		if(a > b && a > c )
		{
			System.out.println(a+ " is largest");
		}
		else if(b > a && b > c)
		{
			System.out.println(b+ " is largest");
		}
		else if(c > a && c > b)
		{
			System.out.println(c+ " is largest");
		}
		else {
			System.out.println("All are equal");
		}
		
		gp.close();
	}

}
