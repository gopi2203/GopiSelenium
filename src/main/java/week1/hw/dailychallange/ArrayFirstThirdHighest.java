package week1.hw.dailychallange;

import java.util.Scanner;

public class ArrayFirstThirdHighest {

	public static void main(String[] args) {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter array size:");
	int arraySize = sc.nextInt();
	
	int[] a = new int[arraySize];
	
	System.out.println("Enter array elements");
	
	for(int k=0;k<arraySize;k++) {
	a[k] = sc.nextInt();
	}
	System.out.println("Enter the largest number position to be displayed:");
		int userInput = sc.nextInt();
		System.out.println(userInput);
		int strlen = a.length;
		int temp=0;
		for(int i=0;i<strlen-1;i++) {
			for(int j=i+1;j<=strlen-1;j++) {
				if(a[i]>a[j]) {
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
		}
	}
}
		System.out.println("numbers in ascending order:");
		for(int i=0;i<strlen;i++) {
				System.out.println(a[i]);
			}
		System.out.println("numbers in descending order:");
			for(int i=strlen-1;i>=0;i--) {
				System.out.println(a[i]);
			}
			
			if(userInput<arraySize) {
				System.out.println("3rd largest number: "+a[strlen-userInput]);
			}
			else {
				System.out.println("Out of range");
			}
			sc.close();
	}
}
