package week5.day1;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STSourceType;
//import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ProjectFindBug {

	@Test
	public void FindBug() throws InterruptedException {

		// launch the browser
		System.setProperty("Webdriver.Chrome.Driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");
		Thread.sleep(3000);
//		driver.manage().window().maximize();

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[text()='Jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span")
				.getText()
				.replaceAll("[^0-9]", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(2000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
				.getText()					
				.replaceAll("[^0-9]", "");
		System.out.println(rightCount);
		Thread.sleep(2000);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//div[@class='product-price']//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();
		String correctPrice;
		for (WebElement productPrice : productsPrice) {
			String allPrice = productPrice.getText();
			correctPrice = allPrice.replaceAll("[^0-9]", "");
			System.out.println(correctPrice);
			onlyPrice.add(correctPrice);
		}
		Collections.sort(onlyPrice);
		for (String allSortedPrice : onlyPrice) {
			System.out.println("the sorted price is "+allSortedPrice);
		}
		int size = onlyPrice.size();
		System.out.println();

		// Sort them 
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);

		// Print Only Allen Solly Brand Minimum Price
		WebElement search = driver.findElementByXPath("//input[@class='desktop-searchBar']");
		Actions allenSolly = builder.moveToElement(search).click().sendKeys(search, "Allen Solly");
		allenSolly.perform();
		Thread.sleep(3000);
		driver.findElementByXPath("(//li[@class='desktop-suggestionTitle'])[2]/following-sibling::li").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//label[text()='Allen Solly']").click();

		JavascriptExecutor js = ((JavascriptExecutor) driver);
		WebElement allenDiscount = 
				driver.findElementByXPath("(//ul[@class='discount-list']/li)[5]");
		js.executeScript("arguments[0].scrollIntoView();", allenDiscount);
		System.out.println("Scrolled down");
//		discountValue.click();
		js.executeScript("arguments[0].click();", allenDiscount);
		System.out.println("clicked successfully");

		List<WebElement> allenProductsPrice = driver.findElementsByXPath("//div[@class='product-price']//span[@class='product-discountedPrice']");
		List<String> allenOnlyPrice = new ArrayList<String>();
		String allenCorrectPrice;
		for (WebElement allenProductPrice : allenProductsPrice) {
			String allenAllPrice = allenProductPrice.getText();
			allenCorrectPrice = allenAllPrice.replaceAll("[^0-9]", "");
			System.out.println(allenCorrectPrice);
			allenOnlyPrice.add(allenCorrectPrice);
		}
		Collections.sort(allenOnlyPrice);
		for (String allenAllSortedPrice : allenOnlyPrice) {
			System.out.println("the sorted allen price is "+allenAllSortedPrice);
		}
		int allenSize = allenOnlyPrice.size();
		System.out.println("The sorted allen size is: "+allenSize);


		//		Get the minimum Price 
		String min = Collections.min(allenOnlyPrice);

		// Find the lowest priced Allen Solly
		System.out.println(min);
























		//		Robot robot = new Robot();
		//		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		//		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
		//		System.out.println("sd........................");
		//		Thread.sleep(2000);
		//				WebElement allenDiscount = 
		//				driver.findElementByXPath("(//ul[@class='discount-list']/li)[5]");
		//				builder.click(allenDiscount).perform();
		//		System.out.println("sdkjfkjdsf...........................");
		//		JavascriptExecutor js = ((JavascriptExecutor) driver);
		//		js.executeScript("window.scrollBy(0,1000)");
		//		WebElement allenDiscount = 
		//				driver.findElementByXPath("//ul[@class='discount-list']");
		//		WebElement discountValue = driver.findElementByXPath("//input[@value='10.0']");
		//		js.executeScript("arguments[0].scrollIntoView();", discountValue);
		//		System.out.println("Scrolled down");
		// discountValue.click();
		//		js.executeScript("arguments[0].click();", discountValue);
		//		System.out.println("clicked successfully");
		//		
		//		js.executeScript("arguments[0].scrollIntoView(true);",allenDiscount);
		//		builder.moveToElement(allenDiscount);
		//		builder.perform();
		//		System.out.println("asdfdsjfdsa.................");
		//		Thread.sleep(2000);
		//		driver.findElementByXPath("//input[@value='50.0']").click();
		//		builder.perform();
		//		Thread.sleep(2000);

		//		Thread.sleep(2000);
		//		System.out.println("sdkjfkjdsf...........................");
		//		allenDiscount.click();
		//		Thread.sleep(2000);
		// Find the costliest Jacket
		//		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		//		List<WebElement> allenProducts = driver.findElementsByXPath("(//ul[@class='results-base']/li)[*]");
		//		int size = allenProducts.size();
		//		System.out.println(size);
		//		for(int i=1; i<=size; i++) {
		//			String allenPrice = driver.findElementByXPath("//ul[@class='results-base']/li["+i+"]//span[@class='product-discountedPrice']").getText();
		//			Thread.sleep(5000);
		//			String correctAllen = allenPrice.replaceAll("[^0-9]+", "");
		//			onlyPrice.add(correctAllen);		
		//		}
		//		Collections.sort(onlyPrice);
		//		
		//		for (String allPrice : onlyPrice) {
		//			System.out.println(allPrice);
		//		}









		//		List<String> allenOnlyPrice = new ArrayList<String>();
		//		System.out.println(productsPrice);
		//
		//		for (WebElement allenProductPrice : productsPrice) {
		//			allenOnlyPrice.add(allenProductPrice.getText().replaceAll("[^0-9]+", ""));
		//		}

		//		// Get the minimum Price 
		//		String min = Collections.min(onlyPrice);
		//
		//		// Find the lowest priced Allen Solly
		//		System.out.println(min);


	}

}
