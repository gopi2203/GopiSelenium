package week5.day1;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {
	
	static ExtentReports extent;
	public static String testCaseName, testCaseDescription , author, category, excelFileName;
	static ExtentTest test;
	
	@BeforeSuite(groups = "common")
	public void StartResult() {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	@BeforeMethod(groups = "common")
	public void StartTestCase() {
		
		test = extent.createTest(testCaseName, testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void reportStep(String desc,String status) {
		if (status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}if (status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		}
}

	
	@AfterSuite(groups = "common")
	public void stopResult() {
		extent.flush();
	}

	

}
