package week5.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import sel.fw.methods.SeMethods;

public class ProjectMyntra extends SeMethods {

	@BeforeClass
	public void setdata() {
		testCaseName = "TC001";
		testCaseDescription =  "Auto Facebook";
		author =  "Gopi";
		category = "Smoke";
	}
	@Test
	public void myntraMen() throws InterruptedException {
		startApp("chrome", "https://www.myntra.com/");
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();
		WebElement jackets = locateElement("linktext", "Jackets");
		click(jackets);
		WebElement countjackets = locateElement("xpath", "//input[@value='Jackets']/following-sibling::span");
		String count = getText(countjackets);
		String correctCount = count.replaceAll("[^0-9]+", "");
		System.out.println(correctCount);
		Thread.sleep(3000);
		WebElement clickJac = locateElement("xpath", "//label[text()='Jackets']");
		click(clickJac);
		WebElement verifyjackets = locateElement("xpath", "//h1[text()='Mens Jackets']/following-sibling::span");
		String verifyCount = getText(verifyjackets);
		String correctVeriCount = verifyCount.replaceAll("[^0-9]+", "");
		System.out.println(correctVeriCount);
		if(correctCount.equals(correctVeriCount)) {
			System.out.println("the Jacket count is verifed successfully");

		}else {
			System.out.println("the jacket count is not matched");
		}
		WebElement offers = locateElement("xpath", "//h4[text()='Offers']");
		click(offers);
		WebElement offers10 = locateElement("xpath", "//input[@value='save10']");
		click(offers10);
		List<WebElement> allList = driver.findElementsByXPath("(//ul[@class='results-base']/li)[*]");
		int size = allList.size();
		System.out.println(size);

		List<WebElement> productsPrice = driver.findElementsByXPath("//div[@class='product-price']//span[@class='product-discountedPrice']");
//		System.out.println(productsPrice);
//		List<String> onlyPrice1 = new ArrayList<String>();
		String correctPrice;
		for (WebElement productPrice : productsPrice) {
			String allPrice = productPrice.getText();
			correctPrice = allPrice.replaceAll("[^0-9]+", "");
			System.out.println(correctPrice);
		}
//		String max = Collections.min(correctPrice);
//		System.out.println(max);
		
		
	}

}
