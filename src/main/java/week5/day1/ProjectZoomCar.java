package week5.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import sel.fw.methods.SeMethods;

public class ProjectZoomCar extends SeMethods {

	@BeforeClass
	public void setdata() {
		testCaseName = "TC001";
		testCaseDescription =  "Auto Facebook";
		author =  "Gopi";
		category = "Smoke";
	}

	@Test
	public void bookZoomCar() throws InterruptedException {
		startApp("chrome", "https://www.zoomcar.com");	
		WebElement chennai = locateElement("xpath", "//img[@alt='Chennai']");
		click(chennai);
		WebElement journey = locateElement("xpath", "//a[@title='Start your wonderful journey']");
		click(journey);
		WebElement place = locateElement("xpath", "//div[contains(text(),'Thuraipakkam')]");
		click(place);
		WebElement next = locateElement("xpath", "//button[text()='Next']");
		click(next);
		WebElement date = locateElement("xpath", "//div[text()='Tue']");
		String bookDate = getText(date);
		System.out.println(bookDate);
		click(date);
		click(next);
		if(bookDate.equalsIgnoreCase("Tue")) {
			System.out.println("the correct date is selected");
		}else {
			System.out.println("the date selected is incorrect");
			click(date);
		}
		WebElement done = locateElement("xpath", "//button[text()='Done']");
		click(done);
		
		List<WebElement> allCars = driver.findElementsByXPath("//div[@class='car-list-layout']/div[*]");
		System.out.println(allCars.size());
		List<WebElement> priceOfCars = driver.findElementsByXPath("//div[@class='car-amount']//div[@class='price']");
		List<String> value= new ArrayList<>();
		String replaceAll;
		for (WebElement Price : priceOfCars) {
			
			String PriceAll = Price.getText();
			replaceAll = PriceAll.replaceAll("[^0-9]+", "");
			System.out.println(replaceAll);
			value.add(replaceAll);
		}	
		Collections.sort(value);
		for (String allSortedValue : value) {
			System.out.println("The Price is "+allSortedValue);
		}
		int size = value.size();
		String highRate = value.get(size-1);
		System.out.println("High rate is: "+highRate);
		String carName = driver.findElementByXPath("//div[contains(text(),"+highRate+")]/preceding::h3").getText();
		System.out.println("Name of the car which has highest rate is: "+carName);
	}

}


