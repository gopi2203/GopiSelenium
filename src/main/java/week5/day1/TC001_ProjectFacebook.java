package week5.day1;



import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sel.fw.methods.SeMethods;

public class TC001_ProjectFacebook extends SeMethods {
	@BeforeClass
	public void setdata() {
				testCaseName = "TC001";
				testCaseDescription =  "Auto Facebook";
				author =  "Gopi";
				category = "Smoke";
	}
@Test
	public void autoFacebook() throws InterruptedException {
		try {
			startApp("chrome", "http://www.facebook.com");
			WebElement faceUser = locateElement("id", "email");
			type(faceUser, "gopinath062@gmail.com");
			WebElement facePass = locateElement("id", "pass");
			type(facePass, "Hashimamla99");
			WebElement faceLogin = locateElement("xpath", "//input[@value='Log In']");
			click(faceLogin);
			WebElement faceSearch = locateElement("xpath", "//input[@data-testid='search_input']");
			type(faceSearch, "TestLeaf");
			WebElement faceClick = locateElement("xpath", "(//button[@type='submit']/i)[1]");
			click(faceClick);
			WebElement faceLike = locateElement("xpath", "(//button[@type='submit']/i)[2]");
			boolean selected = faceLike.isSelected();
			System.out.println(selected);
			if(selected) {
				System.out.println("Like button is already selected");
			}
				else {
					click(faceLike);
					System.out.println("Like button is selected successfully");
				}
			Thread.sleep(3000);
			WebElement faceTestLeaf = locateElement("xpath", "(//div[text()='TestLeaf'])[1]");
			click(faceTestLeaf);
//			Actions builder = new Actions(driver);
//			builder.click(faceTestLeaf).perform();
			WebElement getElement = locateElement("xpath", "(//div[@class='clearfix _ikh']//div)[6]");
			String likeCount = getText(getElement);
			System.out.println(likeCount);
			int size = likeCount.length();
			String a = "";
			for(int i=0;i<size;i++) {
				if(!Character.isLetter(likeCount.charAt(i))) {
				a = a + likeCount.charAt(i);
				}
			}
			System.out.println("the number of likes in the page is "+a);
			verifyTitle("(7) TestLeaf - Home");
			closeBrowser();
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			System.out.println("page not found");
		}
		
		
}		
	}


