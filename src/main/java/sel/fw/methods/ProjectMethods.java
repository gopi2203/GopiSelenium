package sel.fw.methods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.reportExcel;

public class ProjectMethods extends SeMethods {

	@DataProvider(name="fetchdata")
	public Object[][] getData() throws IOException {
		Object [][] data = reportExcel.getExcelData(excelFileName);
		return data;
	}


	@BeforeMethod(groups = "common")
	@Parameters({"browser", "appUrl", "userName", "password"})
	public void login(String browName, String url, String uName, String pwd) {
		startApp(browName, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickLink = locateElement("linktext", "CRM/SFA");
		click(clickLink);
	}

	@AfterMethod(groups = "common")
	public void close() {
		closeBrowser();
	}

	public void createLead() {
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,"CISCO");
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,"Gopinath");
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,"Ekambaram");
	}

}
