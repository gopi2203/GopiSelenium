package sel.fw.methods;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week5.day1.LearnReport;

public class SeMethods extends LearnReport implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions options=new ChromeOptions();
				options.addArguments("--disable-notifications");
				driver = new ChromeDriver(options);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id" : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);

			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		}
		return null;
	}

	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The data "+data+" is Entered Successfully", "PASS");
		} catch (WebDriverException e) {
			reportStep("The data "+data+" is Not Entered","fail");
		} finally {
			takeSnap();
		}
}


	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element clicked successfully", "Pass");
		} catch (Exception e) {
			reportStep("The element is not clicked", "fail");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element clicked successfully", "Pass");
		} catch (WebDriverException e) {
			reportStep("The element is not clicked", "fail");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		String text = null;
		try {
			text=ele.getText();
			reportStep("The text "+text+" printed successfully", "Pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("The text  is not printed successfully", "Fail");
		}
		finally {
			takeSnap();
		}
		return text;
	}

	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportStep("The DropDown Is Selected with VisibleText ","pass");
		} catch (Exception e) {
			reportStep("The DropDown Is not Selected with VisibleText ","fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportStep("The DropDown Is Selected with VisibleText ","pass");
		} catch (Exception e) {
			reportStep("The DropDown Is not Selected with VisibleText ","fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String inputTitle = driver.getTitle();
		// TODO Auto-generated method stub
		try {
		if(inputTitle.equals(expectedTitle)) {
		System.out.println("Titles matched");
		}
		else {
		System.out.println("Titles do not match");
		}
		System.out.println("Title has verified successfully "+inputTitle);
		} catch (Exception e) {
		System.err.println("Title has not verified successfully "+inputTitle);
		}
		return false;
		}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text=ele.getText();
			if(text.equals(expectedText)) {
				System.out.println("Text Matched");
			}
			else {
				System.out.println("Texts do not match");
			}
			reportStep("Text has verified successfully ", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Text has not verified successfully ", "fail");
		}


	}

	public void verifyPartialText(String ele, String expectedText) {
		try {
			if(ele.contains(expectedText)) {
				System.out.println("Text is present");
			}
			else {
				System.out.println("Texts does not present");
			}
			reportStep("Text has verified successfully ", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Text has not verified successfully ","fail");
		}


	}


	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String Strvalue = ele.getAttribute(attribute);
		try {
			if (Strvalue.equals(value)) {
				System.out.println("The Given Attribute Value "+Strvalue+" is available");
			}else {
				System.out.println("The Given Attribute Value "+Strvalue+" is not available");
			}
			reportStep("The element is available under VerifyExactAttribute", "pass");
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			reportStep("The element is not available under VerifyExactAttribute", "fail");
		}catch (Exception e) {
			System.out.println("Something went wrong in VerifyExactAttribute");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String Strvalue = ele.getAttribute(attribute);
		try {
			if (Strvalue.contains(value)) {
				System.out.println("The Given Attribute Value "+Strvalue+" is available");
			}else {
				System.out.println("The Given Attribute Value "+Strvalue+" is not available");
			}
			reportStep("The element is available under VerifyPartialAttribute", "pass");
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			reportStep("The element is not available under VerifyPartialAttribute", "fail");
		}catch (Exception e) {
			System.out.println("Something went wrong in VerifyPartialAttribute");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if(selected) {
				System.out.println("the required checkbox is selected ");
			}
			else {
				System.out.println("the required checkbox is not selected ");
			}
			reportStep("Checkbox is selected successfully", "pass");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			reportStep("No checkbox to select", "fail");
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if(displayed) {
				System.out.println("the required element is visible ");
			}
			else {
				System.out.println("the required element is not visible ");
			}
			reportStep("Element is visible", "pass");
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			reportStep("No such Element", "fail");
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			System.out.println("Count of the windows is: "+allWindows.size());

			List<String> window1 = new ArrayList<String>();
			window1.addAll(allWindows);
			if(index!=0) {
				String childWindow =window1.get(index);
				driver.switchTo().window(childWindow);
				System.out.println("Switching to child window");
			}
			else if(index==0) {
				String parentWindow = window1.get(index);
				System.out.println("Switching to parent window");
				driver.switchTo().window(parentWindow);
			}
			reportStep("Window is navigated successfully", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("No Such Window", "fail");
		}

	}


	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("The child frame is switched successfully ", "pass");
		} catch (NoSuchFrameException e) {
			// TODO Auto-generated catch block
			reportStep("No frame found", "fail");
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			reportStep("Alert handled successfully","pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			reportStep("No Alert is thrown", "fail");
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Alert dismissed successfully", "pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			reportStep("No Alert is thrown", "fail");
		}
	}

	@Override
	public String getAlertText() {
		try {
			String text = driver.switchTo().alert().getText();
			reportStep("the text present in the Alert is printed successfully", "pass");
			return text;
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			reportStep("No Alert is thrown", "fail");
		}
		return null;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			reportStep("IOException", "fail");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			reportStep("The current browser is closed successfully", "pass");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			reportStep("No Window present", "fail");
		}
	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			reportStep("All browser is closed successfully", "pass");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			reportStep("No Window present", "fail");
		}

	}


	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String element = ele.getText();
		try {
			if(element.contains(expectedText)) {
				System.out.println("Text is present");
			}
			else {
				System.out.println("Texts does not present");
			}
			reportStep("Text has verified successfully ", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Text has not verified successfully ", "fail");
		}

	}


	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

}