package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		Thread.sleep(3000);
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		Thread.sleep(3000);
		driver.findElementById("createLeadForm_companyName").sendKeys("CISCO");
		driver.findElementById("createLeadForm_firstName").sendKeys("Gopinath");
		driver.findElementById("createLeadForm_lastName").sendKeys("Ekambaram");
		Thread.sleep(3000);
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		WebElement cnty = driver.findElementById("createLeadForm_generalCountryGeoId");
		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		WebElement markCamp = driver.findElementById("createLeadForm_marketingCampaignId");
		WebElement cntyDollar = driver.findElementById("createLeadForm_currencyUomId");
		WebElement ownerShip = driver.findElementById("createLeadForm_ownershipEnumId");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select dd = new Select(src);
		Select dd1 = new Select(cnty);
		Select dd2 = new Select(state);
		Select dd3 = new Select(markCamp);
		Select dd4 = new Select(cntyDollar);
		Select dd5 = new Select(ownerShip);
		Select dd6 = new Select(industry);
		dd.selectByVisibleText("Direct Mail");
		dd1.selectByValue("IND");
		Thread.sleep(2000);
		dd2.selectByValue("IN-TN");
		dd3.selectByVisibleText("Automobile");
		Thread.sleep(2000);
		dd4.selectByValue("INR");
		dd5.selectByIndex(2);
		Thread.sleep(2000);
		dd6.selectByIndex(7);
		driver.findElementByName("firstNameLocal").sendKeys("Test");
		driver.findElementByName("lastNameLocal").sendKeys("Leaf");
		driver.findElementByName("parentPartyId").sendKeys("");
		driver.findElementByName("personalTitle").sendKeys("Welcome");
		driver.findElementByName("generalProfTitle").sendKeys("Good	");
		driver.findElementByName("departmentName").sendKeys("Finance");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("500000");
		driver.findElementByName("numberEmployees").sendKeys("5999-07");
		driver.findElementByName("tickerSymbol").sendKeys("None");
		driver.findElementByName("description").sendKeys("Test Leaf is good");
		driver.findElementByName("importantNote").sendKeys("The data should be confidential");
		Thread.sleep(2000);
		driver.findElementByName("primaryPhoneAreaCode").sendKeys("2");
		driver.findElementByName("primaryPhoneExtension").sendKeys("5588");
		Thread.sleep(2000);
		driver.findElementByName("primaryEmail").sendKeys("gopi@testleaf.com");
		Thread.sleep(2000);
		driver.findElementByName("primaryPhoneNumber").sendKeys("1234567");
		driver.findElementByName("primaryPhoneAskForName").sendKeys("Aasa");
		driver.findElementByName("primaryWebUrl").sendKeys("http://testleaf.com");
		driver.findElementByName("generalToName").sendKeys("aasa");
		driver.findElementByName("generalAddress1").sendKeys("no 41 A, baharath nagar");
		driver.findElementByName("generalAddress2").sendKeys("Madipakkam");
		driver.findElementByName("generalCity").sendKeys("Chennai");
		driver.findElementByName("generalPostalCode").sendKeys("600091");
		driver.findElementByName("generalPostalCode").sendKeys("600091");
		driver.findElementByName("generalPostalCodeExt").sendKeys("5577");
		driver.findElementByName("generalAttnName").sendKeys("gopi");
		Thread.sleep(4000);
		driver.findElementByClassName("smallSubmit").click();
	}

}