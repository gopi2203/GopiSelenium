package week3.day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FindLeadsUsingXpath {

	public static void main(String[] args) throws IOException {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.get("http://leaftaps.com/opentaps/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
	driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
	driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
	driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
	driver.findElementByXPath("//a[text()='Leads']").click();
	driver.findElementByXPath("//a[text()='Find Leads']").click();
	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Gopi");
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@class,'col-partyId')]//a)[1]")));
	driver.findElementByXPath("(//div[contains(@class,'col-partyId')]//a)[1]").click();
//	File src=driver.getScreenshotAs(OutputType.FILE);
//	File dest = new File("./snapShot/img.png");
//	FileUtils.copyFile(src, dest);
	String leadText=driver.findElementById("viewLead").getText();
	System.out.println("Content of the Lead is: "+leadText);
	if (leadText.contains("Gopi")) {
		System.out.println("Lead is created successfully and it is verified");
	}
	driver.findElementByLinkText("Edit").click();
	}

}
