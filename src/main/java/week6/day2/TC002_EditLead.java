package week6.day2;
import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TC002_EditLead extends ProjectMethods {
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName = "TC003";
		testCaseDescription =  "Merge Lead";
		author =  "Gopi";
		category = "sanity";
		excelFileName = "editlead";
	}
//	@Test(groups = "sanity", dependsOnGroups = "smoke")
	@Test(dataProvider="fetchdata")
//	@Parameters("editLead")
	public void EditLead(String compName,String firstName,String lastName,String marketingCampaign, String generalTitle)
	{
		
		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,compName);
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,firstName);
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,lastName);
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,marketingCampaign);
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");
		WebElement clickEdit = locateElement("linktext", "Edit");
		click(clickEdit);
		WebElement setTitle = locateElement("id","updateLeadForm_generalProfTitle");
		type(setTitle,generalTitle);
		WebElement clickUpdate = locateElement("xpath","//input[@value='Update']");
		click(clickUpdate);
		WebElement printText = locateElement("id", "viewLead");
		getText(printText);
		verifyPartialText(printText,"Editing the existing Lead");
	}

}