package week6.day2;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TC003_DeleteLead extends ProjectMethods {
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName = "TC006";
		testCaseDescription = "Merge Lead";
		author = "Gopi";
		category = "regression";
		excelFileName = "deletelead";
	}
	//	@Test(groups = "regression", dependsOnGroups = "sanity",dataProvider="deleteLead")
	@Test(dataProvider="fetchdata")
	//	@Parameters("deleteLead")
	public void DeleteLead(String compName,String firstName,String lastName,String marketingCampaign)
	{

		WebElement clickLead = locateElement("linktext", "Create Lead");
		click(clickLead);
		WebElement setCompName = locateElement("id", "createLeadForm_companyName");
		type(setCompName,compName);
		WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
		type(setFirstName,firstName);
		WebElement setLastName = locateElement("id", "createLeadForm_lastName");
		type(setLastName,lastName);
		WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markCampDropDown,marketingCampaign);
		// 	selectDropDownUsingIndex(markCampDropDown,5);
		WebElement submitLead = locateElement("class", "smallSubmit");
		click(submitLead);
		WebElement getName = locateElement("name","firstName");
		getText(getName);
		WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(verifyText,"View Lead");
		WebElement getUserId = locateElement("//span[@id='viewLead_companyName_sp']");
		System.out.println(getText(getUserId).replaceAll("[^0-9]", ""));
//		String correctText = text.replaceAll("[^0-9]", "");
//		System.out.println(correctText);
		WebElement clickDelete = locateElement("xpath", "//a[text()='Delete']");
		click(clickDelete);
		WebElement clickFindLead = locateElement("xpath", "//a[text()='Find Leads']");
		click(clickFindLead);
		WebElement getUserID = locateElement("name","id");
//		type(getName, correctText);
		
//		type(locateElement("xpath", "//label[contains(text(),'Lead ID:')]/following::input"), txt);
//		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
//		verifyPartialText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display"); 



	}
}
