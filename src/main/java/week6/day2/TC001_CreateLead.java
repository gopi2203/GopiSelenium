package week6.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import sel.fw.methods.ProjectMethods;
import sel.fw.methods.SeMethods;

public class TC001_CreateLead extends ProjectMethods {
	
	@BeforeClass
	public void setdata() {
				testCaseName = "TC002";
				testCaseDescription =  "Create Lead";
				author =  "Gopi";
				category = "smoke";
				excelFileName = "createlead";
	}
@Test(dataProvider = "fetchdata")
//@Parameters("createlead")
	public void CreateLead(String compName,String firstName,String lastName,String marketingCampaign){
	WebElement clickLead = locateElement("linktext", "Create Lead");
	click(clickLead);
	WebElement setCompName = locateElement("id", "createLeadForm_companyName");
	type(setCompName,compName);
	WebElement setFirstName = locateElement("id", "createLeadForm_firstName");
	type(setFirstName,firstName);
	WebElement setLastName = locateElement("id", "createLeadForm_lastName");
	type(setLastName,lastName);
	WebElement markCampDropDown = locateElement("id","createLeadForm_marketingCampaignId");
	selectDropDownUsingText(markCampDropDown,marketingCampaign);
	WebElement submitLead = locateElement("class", "smallSubmit");
	click(submitLead);
	WebElement printText = locateElement("id", "viewLead");
	getText(printText);
	WebElement verifyText = locateElement("id","sectionHeaderTitle_leads");
	verifyExactText(verifyText,"View Lead");


}





}
